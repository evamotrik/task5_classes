//User


class User {
    constructor(loginStatus, registerDate) {
        this._userId = "";
        this._password = "";
        this.loginStatus = String(loginStatus);
        this.registerDate = new Date(registerDate);
    }
    
    set userId(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._userId = val;
    }

    get userId() {
        return this._userId;
    }

    set password(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._password = val;
    }

    get password() {
        return this._password;
    }

    verifyLogin() {
       if (this.loginStatus == 0) {
           return false
        }
        return true
    };
}

const user = new User("loginStatus", "2017-01-26");

try {
    user.userId = "userId";
    user._password = "pass";
}
catch (e) {
    console.log(e.message);
}

console.log(user);

//Administrator

class Administrator {
    constructor() {
        this._adminName = "";
        this._email = "";
    }
    set adminName(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._adminName = val;
    }

    get adminName() {
        return this._adminName;
    }

    set email(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._email = val;
    }

    get email() {
        return this._email;
    }

    updateCatalog() {
        if (this.adminName == 0) {
            return false
         }
         return true
     };
}

const admin = new Administrator();

try {
    admin.adminName = "admin";
    admin.email = "email@.com"
}
catch (e) {
    console.log(e.message);
}

console.log(admin);

//Customer

class Customer {
    constructor(address, creditCardInfo, shippingInfo, accountBalance) {
        this._customerName = "";
        this._email = "";
        this.address = String(address);
        this.creditCardInfo = String(creditCardInfo);
        this.shippingInfo = String(shippingInfo);
        this.accountBalance = parseFloat(accountBalance); 
    }
    set customerName(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._customerName = val;
    }

    get customerName() {
        return this._customerName;
    }

    set email(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._email = val;
    }

    get email() {
        return this._email;
    }
    register(){}
    login(){}
    updateProfile(){}
}

const customer = new Customer("Address", "creditCardInfo", "shippingInfo", "8.");

try {
    customer.customerName = "customer";
    customer.email = "customeremail@.com"
}
catch (e) {
    console.log(e.message);
}

console.log(customer);

//ShoppingCart

class ShoppingCart {
    constructor(quantity, dateAdded) {
        this._cartId = "";
        this._productId = "";
        this.quantity = Number(quantity);
        this.dateAdded = Number(dateAdded);
    }
    set cartId(val) {
        if (isNaN(val) || val == "") {
            throw new Error("Invalide type. Should be number")
        }
        this._cartId = val;
    }

    get cartId() {
        return this._cartId;
    }

    set productId(val) {
        if (isNaN(val) || val == "") {
            throw new Error("Invalide type. Should be number")
        }
        this._productId = val;
    }

    get productId() {
        return this._productId;
    }
    addCartItem(){};
    updateQuantity(){};
    viewCartDetails(){};
    chekOut(){};
}

const shoppingCart = new ShoppingCart("5", "5");

try {
    shoppingCart.cartId = "123";
    shoppingCart._productId = "321"
}
catch (e) {
    console.log(e.message);
}

console.log(shoppingCart);

//Order

class Order {
    constructor(dateCreated, dateShipped, customerName, customerId, status) {
        this._orderId = "";
        this._dateCreated = "";
        this._shippingId = String(this.shippingId);
        this.dateShipped = String(dateShipped);
        this.customerName = String(customerName);
        this.customerId = String(customerId);
        this.status = String(status);
    }
    set orderId(val) {
        if (isNaN(val) || val == "") {
            throw new Error("Invalide type. Should be number")
        }
        this._orderId = val;
    }

    get orderId() {
        return this._orderId;
    }

    set dateCreated(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._dateCreated = val;
    }

    get dateCreated() {
        return this._dateCreated;
    }
    placeOrder(){};
}

const order = new Order("1", "2", "3", "4", "5");

try {
    order._orderId = "123";
    order._dateCreated = "aaa"
}
catch (e) {
    console.log(e.message);
}

console.log(order);

//Shipping Info

class ShippingInfo {
    constructor(shippingCost, shippingRegionid) {
        this._shippingId = "";
        this._shippingType = "";
        this.shippingCost = Number(shippingCost);
        this.shippingRegionid = Number(shippingRegionid);
    }
    set shippingId(val) {
        if (isNaN(val) || val == "") {
            throw new Error("Invalide type. Should be number")
        }
        this._shippingId = val;
    }

    get shippingId() {
        return this._shippingId;
    }

    set shippingType(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._shippingType = val;
    }

    get shippingType() {
        return this._shippingType;
    }
    updateShippingInfo(){};
}

const shippingInfo = new ShippingInfo("1", "2");

try {
    shippingInfo._shippingId = "aaa";
    shippingInfo._shippingType = "123";
}
catch (e) {
    console.log(e.message);
}

console.log(shippingInfo);

//OrderDetails

class OrderDetails {
    constructor(productName, quantity, unitCost, subtotal) {
        this._orderId = "";
        this._productId = "";
        this.productName = String(productName);
        this.squantity = Number(quantity);
        this.unitCost = parseFloat(unitCost);
        this.subtotal = parseFloat(subtotal);
    }
    set orderId(val) {
        if (isNaN(val) || val == "") {
            throw new Error("Invalide type. Should be number")
        }
        this._orderId = val;
    }

    get orderId() {
        return this._orderId;
    }

    set productId(val) {
        if (typeof val !== "string") {
            throw new Error("Invalide type. Should be string")
        }
        this._productId = val;
    }

    get productId() {
        return this._productId;
    }
    calcPrice(){};
}

const orderDetails = new OrderDetails("s", "2", "4", "5.7", "9.4");

try {
    orderDetails._orderId = "aaa";
    orderDetails._productId = "123";
}
catch (e) {
    console.log(e.message);
}

console.log(orderDetails);

